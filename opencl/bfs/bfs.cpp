//--by Jianbin Fang

#define __CL_ENABLE_EXCEPTIONS
#include <cstdlib>
#include <iostream>
#include <string>
#include <cstring>
#include <math.h>
//#define PROFILING true
//#ifdef PROFILING
#include "timer.h"

//#endif

#include "util.h"

#include "CLHelper.h"

#define MAX_THREADS_PER_BLOCK 256

int no_of_nodes;
int edge_list_size;
FILE *fp;

// Structure to hold a node information
struct Node {
    int starting;
    int no_of_edges;
};

void BFSGraph(int argc, char **argv);

void Usage(int argc, char **argv) {

    fprintf(stderr, "Usage: %s <input_file>\n", argv[0]);
}


////////////////////////////////////////////////////////////////////////////////
// Main Program
////////////////////////////////////////////////////////////////////////////////
int main(int argc, char **argv) {
    no_of_nodes = 0;
    edge_list_size = 0;
    BFSGraph(argc, argv);
}


////////////////////////////////////////////////////////////////////////////////
// Apply BFS on a Graph
////////////////////////////////////////////////////////////////////////////////
void BFSGraph(int argc, char **argv) {

	double start_timer;
	double end;
    char *input_f;

    if (argc != 2) {
        Usage(argc, argv);
        exit(0);
    }

    input_f = argv[1];

    printf("Reading File\n");
    // Read in Graph from a file
    fp = fopen(input_f, "r");
    if (!fp) {
        printf("Error Reading graph file\n");
        return;
    }

    int source = 0;

    fscanf(fp, "%d", &no_of_nodes);

    int num_of_blocks = 1;
    int num_of_threads_per_block = no_of_nodes;

    // Make execution Parameters according to the number of nodes
    // Distribute threads across multiple Blocks if necessary
    if (no_of_nodes > MAX_THREADS_PER_BLOCK) {
        num_of_blocks = (int)ceil(no_of_nodes / (double)MAX_THREADS_PER_BLOCK);
        num_of_threads_per_block = MAX_THREADS_PER_BLOCK;
    }
    work_group_size = num_of_threads_per_block;

    	start_timer = gettime();
    // allocate host memory
    Node *h_graph_nodes = (Node *)malloc(sizeof(Node) * no_of_nodes);
    char *h_graph_mask = (char *)malloc(sizeof(char) * no_of_nodes);
    char *h_updating_graph_mask = (char *)malloc(sizeof(char) * no_of_nodes);
    char *h_graph_visited = (char *)malloc(sizeof(char) * no_of_nodes);
	end = gettime();
	printf("Allocate host memory in %lf\n", (end-start_timer));

	start_timer = gettime();
    int start, edgeno;
    // initalize the memory
    for (unsigned int i = 0; i < no_of_nodes; i++) {
        fscanf(fp, "%d %d", &start, &edgeno);
        h_graph_nodes[i].starting = start;
        h_graph_nodes[i].no_of_edges = edgeno;
        h_graph_mask[i] = false;
        h_updating_graph_mask[i] = false;
        h_graph_visited[i] = false;
    }
	end = gettime();
	printf("Memory initialized in %lf\n", (end-start_timer));

    // read the source node from the file
    fscanf(fp, "%d", &source);
    // source=0; //tesing code line

    // set the source node as true in the mask
    h_graph_mask[source] = true;
    h_graph_visited[source] = true;

    fscanf(fp, "%d", &edge_list_size);
	start_timer = gettime();
    int id, cost;
    int *h_graph_edges = (int *)malloc(sizeof(int) * edge_list_size);
    for (int i = 0; i < edge_list_size; i++) {
        fscanf(fp, "%d", &id);
        fscanf(fp, "%d", &cost);
        h_graph_edges[i] = id;
    }
	end = gettime();
	printf("File read in %lf\n", (end-start_timer));

    if (fp)
        fclose(fp);

	start_timer = gettime();
    // allocate mem for the result on host side
    int *h_cost = (int *)malloc(sizeof(int) * no_of_nodes);
    for (int i = 0; i < no_of_nodes; i++)
        h_cost[i] = -1;
    h_cost[source] = 0;
    // int number_elements = height*width;
    char h_over;
    cl_mem d_graph_nodes, d_graph_edges, d_graph_mask, d_updating_graph_mask,
        d_graph_visited, d_cost, d_over;
    //--1 transfer data from host to device
    _clInit();
    d_graph_nodes = _clMalloc(no_of_nodes * sizeof(Node), h_graph_nodes);
    d_graph_edges = _clMalloc(edge_list_size * sizeof(int), h_graph_edges);
    d_graph_mask = _clMallocRW(no_of_nodes * sizeof(char), h_graph_mask);
    d_updating_graph_mask =
        _clMallocRW(no_of_nodes * sizeof(char), h_updating_graph_mask);
    d_graph_visited = _clMallocRW(no_of_nodes * sizeof(char), h_graph_visited);


    d_cost = _clMallocRW(no_of_nodes * sizeof(int), h_cost);
    d_over = _clMallocRW(sizeof(char), &h_over);

    _clMemcpyH2D(d_graph_nodes, no_of_nodes * sizeof(Node), h_graph_nodes);
    _clMemcpyH2D(d_graph_edges, edge_list_size * sizeof(int), h_graph_edges);
    _clMemcpyH2D(d_graph_mask, no_of_nodes * sizeof(char), h_graph_mask);
    _clMemcpyH2D(d_updating_graph_mask, no_of_nodes * sizeof(char),
                 h_updating_graph_mask);
    _clMemcpyH2D(d_graph_visited, no_of_nodes * sizeof(char), h_graph_visited);
    _clMemcpyH2D(d_cost, no_of_nodes * sizeof(int), h_cost);
	end = gettime();
	printf("Transfer data from host to device in %lf\n", (end-start_timer));

	start_timer = gettime();
//--2 invoke kernel
//#ifdef PROFILING
    timer kernel_timer;
    double kernel_time = 0.0;
    kernel_timer.reset();
    kernel_timer.start();
//#endif
	double avarage=0;
	int counter=0;
	double start1, end1;
	cl_event kernelEvent1, kernelEvent2;
	cl_ulong eventStart, eventEnd = 0;

    do {
	    start1 = gettime();
        h_over = false;
        _clMemcpyH2D(d_over, sizeof(char), &h_over);
        //--kernel 0
        int kernel_id = 0;
        int kernel_idx = 0;
        _clSetArgs(kernel_id, kernel_idx++, d_graph_nodes);
        _clSetArgs(kernel_id, kernel_idx++, d_graph_edges);
        _clSetArgs(kernel_id, kernel_idx++, d_graph_mask);
        _clSetArgs(kernel_id, kernel_idx++, d_updating_graph_mask);
        _clSetArgs(kernel_id, kernel_idx++, d_graph_visited);
        _clSetArgs(kernel_id, kernel_idx++, d_cost);
        _clSetArgs(kernel_id, kernel_idx++, &no_of_nodes, sizeof(int));

        // int work_items = no_of_nodes;
        _clInvokeKernel(kernel_id, no_of_nodes, work_group_size, &kernelEvent1);

        //--kernel 1
        kernel_id = 1;
        kernel_idx = 0;
        _clSetArgs(kernel_id, kernel_idx++, d_graph_mask);
        _clSetArgs(kernel_id, kernel_idx++, d_updating_graph_mask);
        _clSetArgs(kernel_id, kernel_idx++, d_graph_visited);
        _clSetArgs(kernel_id, kernel_idx++, d_over);
        _clSetArgs(kernel_id, kernel_idx++, &no_of_nodes, sizeof(int));

        // work_items = no_of_nodes;
        _clInvokeKernel(kernel_id, no_of_nodes, work_group_size, &kernelEvent2);

        _clMemcpyD2H(d_over, sizeof(char), &h_over);
	end1 = gettime();
	avarage += (end1 - start1);
	counter++;
    } while (h_over);

    _clFinish();
//#ifdef PROFILING
    kernel_timer.stop();
    kernel_time = kernel_timer.getTimeInSeconds();
//#endif
	end = gettime();
	printf("Kernel calls finished in %lf\n", (end-start_timer));
	printf("A kernel executed in avarage of %lf\n", (end1-start1));
	start_timer = gettime();
    //--3 transfer data from device to host
    _clMemcpyD2H(d_cost, no_of_nodes * sizeof(int), h_cost);
	end = gettime();
	printf("Data copied from device to host in %lf\n", (end-start_timer));
    	
//--statistics
//#ifdef PROFILING
    std::cout << "kernel time(s):" << kernel_time << std::endl;
//#endif

    // Store the result into a file
    if (getenv("OUTPUT")) {
        FILE *fpo = fopen("outputShort.txt", "w");
        for (int i = 0; i < no_of_nodes; i++)
            fprintf(fpo, "%d) cost:%d\n", i, h_cost[i]);
        fclose(fpo);
    }else if (getenv("OUTPUT2")) {
        FILE *fpo = fopen("outputMedium.txt", "w");
        for (int i = 0; i < no_of_nodes; i++)
            fprintf(fpo, "%d) cost:%d\n", i, h_cost[i]);
        fclose(fpo);
    }else if (getenv("OUTPUT3")) {
        FILE *fpo = fopen("outputLong.txt", "w");
        for (int i = 0; i < no_of_nodes; i++)
            fprintf(fpo, "%d) cost:%d\n", i, h_cost[i]);
        fclose(fpo);
    }
    //kernel timing
    //clFinish(cmd_queue);
    cl_int err = 0;
	err = clGetEventProfilingInfo(kernelEvent1, CL_PROFILING_COMMAND_START,
					sizeof(cl_ulong), &eventStart, NULL);
	if (err != CL_SUCCESS) {
		printf ("Kernel timing start failed error: %d\n", err);
	}
	err = clGetEventProfilingInfo(kernelEvent1, CL_PROFILING_COMMAND_END,
					sizeof(cl_ulong), &eventEnd, NULL);
	if (err != CL_SUCCESS) {
		printf ("Kernel timing end failed\n");
	}
	printf("*******Kernel Event 1 executed in: %f\n",(float)((eventEnd - eventStart) / 1e9));
	err = clGetEventProfilingInfo(kernelEvent2, CL_PROFILING_COMMAND_START,
					sizeof(cl_ulong), &eventStart, NULL);
	if (err != CL_SUCCESS) {
		printf ("Kernel timing start failed error: %d\n", err);
	}
	err = clGetEventProfilingInfo(kernelEvent2, CL_PROFILING_COMMAND_END,
					sizeof(cl_ulong), &eventEnd, NULL);
	if (err != CL_SUCCESS) {
		printf ("Kernel timing end failed\n");
	}
	printf("*******Kernel Event 2 executed in: %f\n", (float)((eventEnd - eventStart) / 1e9));


	start_timer = gettime();
    // cleanup memory
    free(h_graph_nodes);
    free(h_graph_edges);
    free(h_graph_mask);
    free(h_updating_graph_mask);
    free(h_graph_visited);
    free(h_cost);
    _clFree(d_graph_nodes);
    _clFree(d_graph_edges);
    _clFree(d_graph_mask);
    _clFree(d_updating_graph_mask);
    _clFree(d_graph_visited);
    _clFree(d_cost);
    _clFree(d_over);
    _clRelease();
	end = gettime();
	printf("Cleanup memory executed in %lf\n", (end-start_timer));
}
