OPENMP_DIR := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
include $(OPENMP_DIR)/../common.mk
CPPFLAGS += -I/usr/local/cuda-11.2/include/
CPPFLAGS += -L/usr/local/cuda-11.2/targets/x86_64-linux/lib/
CFLAGS 	 += -I/usr/local/cuda-11.2/include/
LDLIBS   +=  -lOpenCL
