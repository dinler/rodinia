#ifndef _C_UTIL_
#define _C_UTIL_
#include <math.h>
#include <omp.h>
#include <sys/time.h>


double gettime() {
    struct timeval t;
    gettimeofday(&t, NULL);
    return t.tv_sec + t.tv_usec * 1e-6;
}

#endif
