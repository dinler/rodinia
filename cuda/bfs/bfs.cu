/***********************************************************************************
  Implementing Breadth first search on CUDA using algorithm given in HiPC'07
  paper "Accelerating Large Graph Algorithms on the GPU using CUDA"

  Copyright (c) 2008 International Institute of Information Technology -
 Hyderabad.
  All rights reserved.

  Permission to use, copy, modify and distribute this software and its
 documentation for
  educational purpose is hereby granted without fee, provided that the above
 copyright
  notice and this permission notice appear in all copies of this software and
 that you do
  not sell the software.

  THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT WARRANTY OF ANY KIND,EXPRESS,
 IMPLIED OR
  OTHERWISE.

  Created by Pawan Harish.
 ************************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <cuda.h>

#include "../../common/cuda/profile_main.h"
#include "../common/common.h"

#define MAX_THREADS_PER_BLOCK 512

int no_of_nodes;
int edge_list_size;
FILE *fp;

// Structure to hold a node information
struct Node {
    int starting;
    int no_of_edges;
};

#define gpuErrchk(ans) { gpuAssert((ans),__FILE__, __LINE__ ); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
	if (code != cudaSuccess)
	{
		fprintf(stderr, "GPUasser: %s %s %d\n", cudaGetErrorString(code), file, line);
		if (abort) exit(code);
	}
}

#include "kernel.cu"
#include "kernel2.cu"

void run(int argc, char **argv);

void Usage(int argc, char **argv) {

    fprintf(stderr, "Usage: %s <input_file>\n", argv[0]);
}


////////////////////////////////////////////////////////////////////////////////
// Main Program
////////////////////////////////////////////////////////////////////////////////

int main(int argc, char **argv) {
    //stopwatch sw;
    //stopwatch_start(&sw);
    run(argc, argv);
    //stopwatch_stop(&sw);
    //printf("First main executed in (ms): %lf\n", 1000 * get_interval_by_sec(&sw));
    /*if (getenv("TWICE")) {
    	stopwatch_start(&sw);
    	run(argc, argv);
    	stopwatch_stop(&sw);
    	printf("Second main executed in (ms): %lf\n", 1000 * get_interval_by_sec(&sw));
    }*/
    if (getenv("PROFILE")) {
        // warm up
        for (int i = 0; i < 5; i++)
            run(argc, argv);

        checkCudaErrors(cudaProfilerStart());
        nvtxRangePushA("host");

        for (int i = 0; i < 5; i++)
        	run(argc, argv);

        nvtxRangePop();
        checkCudaErrors(cudaProfilerStop());
    }

    return EXIT_SUCCESS;
}


////////////////////////////////////////////////////////////////////////////////
// Apply BFS on a Graph
////////////////////////////////////////////////////////////////////////////////
void run(int argc, char **argv) {
    char *input_f;
    no_of_nodes = 0;
    edge_list_size = 0;

    if (argc != 2) {
        Usage(argc, argv);
        exit(0);
    }

    input_f = argv[1];

    //stopwatch sw;
    
    printf("Reading File\n");
    // Read in Graph from a file
    fp = fopen(input_f, "r");
    if (!fp) {
        printf("Error Reading graph file\n");
        return;
    }

    int source = 0;

    fscanf(fp, "%d", &no_of_nodes);

    int num_of_blocks = 1;
    int num_of_threads_per_block = no_of_nodes;

    // Make execution Parameters according to the number of nodes
    // Distribute threads across multiple Blocks if necessary
    if (no_of_nodes > MAX_THREADS_PER_BLOCK) {
        num_of_blocks = (int)ceil(no_of_nodes / (double)MAX_THREADS_PER_BLOCK);
        num_of_threads_per_block = MAX_THREADS_PER_BLOCK;
    }

    //stopwatch_start(&sw);
    // allocate host memory
    Node *h_graph_nodes = (Node *)malloc(sizeof(Node) * no_of_nodes);
    bool *h_graph_mask = (bool *)malloc(sizeof(bool) * no_of_nodes);
    bool *h_updating_graph_mask = (bool *)malloc(sizeof(bool) * no_of_nodes);
    bool *h_graph_visited = (bool *)malloc(sizeof(bool) * no_of_nodes);
    //stopwatch_stop(&sw);
    //printf("Allocate host memory in (ms): %lf\n", 1000 * get_interval_by_sec(&sw));


    //stopwatch_start(&sw);
    int start, edgeno;
    // initalize the memory
    for (unsigned int i = 0; i < no_of_nodes; i++) {
        fscanf(fp, "%d %d", &start, &edgeno);
        h_graph_nodes[i].starting = start;
        h_graph_nodes[i].no_of_edges = edgeno;
        h_graph_mask[i] = false;
        h_updating_graph_mask[i] = false;
        h_graph_visited[i] = false;
    }
    //stopwatch_stop(&sw);
    //printf("Memory initialized in (ms): %lf\n", 1000 * get_interval_by_sec(&sw));

    // read the source node from the file
    fscanf(fp, "%d", &source);
    // source=0; //tesing code line

    // set the source node as true in the mask
    h_graph_mask[source] = true;
    h_graph_visited[source] = true;

    fscanf(fp, "%d", &edge_list_size);
	printf("EDGE SIZE: %d\n", edge_list_size);
    //stopwatch_start(&sw);
    int id, cost;
    int *h_graph_edges = (int *)malloc(sizeof(int) * edge_list_size);
    for (int i = 0; i < edge_list_size; i++) {
        fscanf(fp, "%d", &id);
        fscanf(fp, "%d", &cost);
        h_graph_edges[i] = id;
    }
    //stopwatch_stop(&sw);
    //printf("File read in (ms): %lf\n", 1000 * get_interval_by_sec(&sw));
    

    if (fp)
        fclose(fp);

    //printf("Read File\n");

    //stopwatch_start(&sw);
    // Copy the Node list to device memory
    Node *d_graph_nodes;
    cudaMalloc((void **)&d_graph_nodes, sizeof(Node) * no_of_nodes);
    cudaMemcpy(d_graph_nodes, h_graph_nodes, sizeof(Node) * no_of_nodes,
               cudaMemcpyHostToDevice);

    size_t totalDeviceMemory;
    size_t freeDeviceMemory;
    //unsigned long usableDeviceMemory = freeDeviceMemory * 85 / 100;
    cudaMemGetInfo(&freeDeviceMemory, &totalDeviceMemory);
	unsigned long usableDeviceMemory = freeDeviceMemory * 85 / 100;	
    printf("Total Device Memory: %d\nFree Device Memory: %d\nUsable Device memory: %d\n", totalDeviceMemory, freeDeviceMemory, usableDeviceMemory);
    // Copy the Edge List to device Memory
    int *d_graph_edges;
    cudaMalloc((void **)&d_graph_edges, sizeof(int) * edge_list_size);
    cudaMemcpy(d_graph_edges, h_graph_edges, sizeof(int) * edge_list_size,
               cudaMemcpyHostToDevice) ;

    // Copy the Mask to device memory
    bool *d_graph_mask;
    cudaMalloc((void **)&d_graph_mask, sizeof(bool) * no_of_nodes) ;
    cudaMemcpy(d_graph_mask, h_graph_mask, sizeof(bool) * no_of_nodes,
               cudaMemcpyHostToDevice) ;

    bool *d_updating_graph_mask;
    cudaMalloc((void **)&d_updating_graph_mask, sizeof(bool) * no_of_nodes) ;
    cudaMemcpy(d_updating_graph_mask, h_updating_graph_mask,
               sizeof(bool) * no_of_nodes, cudaMemcpyHostToDevice) ;

    // Copy the Visited nodes array to device memory
    bool *d_graph_visited;
    cudaMalloc((void **)&d_graph_visited, sizeof(bool) * no_of_nodes) ;
    cudaMemcpy(d_graph_visited, h_graph_visited, sizeof(bool) * no_of_nodes,
               cudaMemcpyHostToDevice) ;

    // allocate mem for the result on host side
    int *h_cost = (int *)malloc(sizeof(int) * no_of_nodes);
    for (int i = 0; i < no_of_nodes; i++)
        h_cost[i] = -1;
    h_cost[source] = 0;

    // allocate device memory for result
    int *d_cost;
    cudaMalloc((void **)&d_cost, sizeof(int) * no_of_nodes) ;
    cudaMemcpy(d_cost, h_cost, sizeof(int) * no_of_nodes,
               cudaMemcpyHostToDevice) ;

    
    //stopwatch_stop(&sw);
    //printf("Manual copies to device in (ms): %lf\n", 1000 * get_interval_by_sec(&sw));

    // make a bool to check if the execution is over
    bool *d_over;
    cudaMalloc((void **)&d_over, sizeof(bool)) ;

    printf("Copied Everything to GPU memory\n");
	printf("num_of_blocks: %d\n", num_of_blocks);
    // setup execution parameters
    dim3 grid(num_of_blocks, 1, 1);
    dim3 threads(num_of_threads_per_block, 1, 1);

    //stopwatch_start(&sw);
    int k = 0;
    printf("Start traversing the tree\n");
    bool stop;
    // Call the Kernel untill all the elements of Frontier are not false
    do {
        // if no thread changes this value then the loop stops
        stop = false;
        cudaMemcpy(d_over, &stop, sizeof(bool), cudaMemcpyHostToDevice);

        PROFILE((
            Kernel<<<grid, threads, 0>>>(d_graph_nodes, d_graph_edges, d_graph_mask,
                                         d_updating_graph_mask, d_graph_visited,
                                         d_cost, no_of_nodes)
        ));

        PROFILE((
            Kernel2<<<grid, threads, 0>>>(d_graph_mask, d_updating_graph_mask,
                                          d_graph_visited, d_over, no_of_nodes)
        ));

        cudaMemcpy(&stop, d_over, sizeof(bool), cudaMemcpyDeviceToHost);
        k++;
    } while (stop);
	
    //stopwatch_stop(&sw);
    //printf("Kernel calls finished  in (ms): %lf\n", 1000 * get_interval_by_sec(&sw));


    printf("Kernel Executed %d times\n", k);

    //stopwatch_start(&sw);
    // copy result from device to host
    cudaMemcpy(h_cost, d_cost, sizeof(int) * no_of_nodes,
               cudaMemcpyDeviceToHost) ;
    //stopwatch_stop(&sw);
    //printf("Cuda from device to host in (ms): %lf\n", 1000 * get_interval_by_sec(&sw));
	

    if (getenv("OUTPUT")) {
        FILE *fpo = fopen("outputShort.txt", "w");
        for (int i = 0; i < no_of_nodes; i++)
            fprintf(fpo, "%d) cost:%d\n", i, h_cost[i]);
        fclose(fpo);
    }else if ( getenv("OUTPUT2")) {
        FILE *fpo = fopen("outputMedium.txt", "w");
        for (int i = 0; i < no_of_nodes; i++)
            fprintf(fpo, "%d) cost:%d\n", i, h_cost[i]);
        fclose(fpo);
    }else if ( getenv("OUTPUT3")) {
        FILE *fpo = fopen("outputLong.txt", "w");
        for (int i = 0; i < no_of_nodes; i++)
            fprintf(fpo, "%d) cost:%d\n", i, h_cost[i]);
        fclose(fpo);
    }

	//stopwatch_start(&sw);
    // cleanup memory
    free(h_graph_nodes);
    free(h_graph_edges);
    free(h_graph_mask);
    free(h_updating_graph_mask);
    free(h_graph_visited);
    free(h_cost);
    cudaFree(d_graph_nodes);
    cudaFree(d_graph_edges);
    cudaFree(d_graph_mask);
    cudaFree(d_updating_graph_mask);
    cudaFree(d_graph_visited);
    cudaFree(d_cost);
    
    //stopwatch_stop(&sw);
    //printf("Cleanup memory completed in (ms): %lf\n", 1000 * get_interval_by_sec(&sw));
}
