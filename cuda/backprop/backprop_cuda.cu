

// includes, system
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <cuda.h>
#include <sys/time.h>

// includes, kernels
#include "backprop_cuda_kernel.cu"
#include "backprop.h"

#include "../../common/cuda/profile_main.h"

#include "../common/common.h"

////////////////////////////////////////////////////////////////////////////////

extern "C" void bpnn_layerforward(float *l1, float *l2, float **conn, int n1,
                                  int n2);

extern "C" void bpnn_output_error(float *delta, float *target, float *output,
                                  int nj, float *err);

extern "C" void bpnn_hidden_error(float *delta_h, int nh, float *delta_o,
                                  int no, float **who, float *hidden,
                                  float *err);

extern "C" void bpnn_adjust_weights(float *delta, int ndelta, float *ly,
                                    int nly, float **w, float **oldw);


extern "C" int run(int argc, char **argv);

extern "C" float **alloc_2d_dbl(int m, int n);

extern "C" float squash(float x);

double gettime() {
    struct timeval t;
    gettimeofday(&t, NULL);
    return t.tv_sec + t.tv_usec * 1e-6;
}

unsigned int num_threads = 0;
unsigned int num_blocks = 0;

////////////////////////////////////////////////////////////////////////////////
// Program main
////////////////////////////////////////////////////////////////////////////////
int main(int argc, char **argv) {
	stopwatch sw_main;
	stopwatch_start(&sw_main);
    run(argc, argv);
    	stopwatch_stop(&sw_main);
    	printf("First main executed in (ms): %lf\n", 1000 * get_interval_by_sec(&sw_main));

	if (getenv("TWICE")){
	stopwatch_start(&sw_main);
    run(argc, argv);
    	stopwatch_stop(&sw_main);
    	printf("Second main executed in (ms): %lf\n", 1000 * get_interval_by_sec(&sw_main));
    	}

    if (getenv("PROFILE")) {
        // warm up
        for (int i = 0; i < 5; i++)
            run(argc, argv);

        checkCudaErrors(cudaProfilerStart());
        nvtxRangePushA("host");

        for (int i = 0; i < 5; i++)
        	run(argc, argv);

        nvtxRangePop();
        checkCudaErrors(cudaProfilerStop());
    }

    return EXIT_SUCCESS;
}


extern "C" void bpnn_train_cuda(BPNN *net, float *eo, float *eh) {
	stopwatch sw;
	stopwatch_start(&sw);
    int in, hid, out;
    float out_err, hid_err;

    in = net->input_n;
    hid = net->hidden_n;
    out = net->output_n;

#ifdef GPU
    int m = 0;
    float *input_hidden_cuda;
    float *input_cuda;
    float *output_hidden_cuda;
    float *partial_sum;
    float *hidden_partial_sum;
    float *hidden_delta_cuda;
    float *input_prev_weights_cuda;
    float sum;
    float *input_weights_one_dim;
    float *input_weights_prev_one_dim;
    num_blocks = in / 16;
    dim3 grid(1, num_blocks);
    dim3 threads(16, 16);
	printf("in : %d, num_blocks: %d\n", in, num_blocks);
    input_weights_one_dim =
        (float *)malloc((in + 1) * (hid + 1) * sizeof(float));
    input_weights_prev_one_dim =
        (float *)malloc((in + 1) * (hid + 1) * sizeof(float));
    partial_sum = (float *)malloc(num_blocks * WIDTH * sizeof(float));
	stopwatch_stop(&sw);
    	printf("Input weights and partial sum matrix allocation executed in (ms): %lf\n", 1000 * get_interval_by_sec(&sw));

	stopwatch_start(&sw);
    // this preprocessing stage is added to correct the bugs of wrong memcopy
    // using two-dimensional net->inputweights
    for (int k = 0; k <= in; k++) {
        for (int j = 0; j <= hid; j++) {
            input_weights_one_dim[m] = net->input_weights[k][j];
            input_weights_prev_one_dim[m] = net->input_prev_weights[k][j];
            m++;
        }
    }
	stopwatch_stop(&sw);
    	printf("Preprocessing executed in (ms): %lf\n", 1000 * get_interval_by_sec(&sw));

	stopwatch_start(&sw);
    cudaMalloc((void **)&input_cuda, (in + 1) * sizeof(float));
    cudaMalloc((void **)&output_hidden_cuda, (hid + 1) * sizeof(float));
    cudaMalloc((void **)&input_hidden_cuda,
               (in + 1) * (hid + 1) * sizeof(float));
    cudaMalloc((void **)&hidden_partial_sum,
               num_blocks * WIDTH * sizeof(float));

    	stopwatch_stop(&sw);
    	printf("CUDA Allocation executed in (ms): %lf\n", 1000 * get_interval_by_sec(&sw));

#endif

#ifdef CPU

    printf("Performing CPU computation\n");
    stopwatch_start(&sw);
    bpnn_layerforward(net->input_units, net->hidden_units, net->input_weights,
                      in, hid);
    
	stopwatch_stop(&sw);
    	printf("CPU Computation executed in (ms): %lf\n", 1000 * get_interval_by_sec(&sw));

#endif

#ifdef GPU

    printf("Performing GPU computation\n");
	stopwatch_start(&sw);
    // printf("in= %d, hid = %d, numblocks = %d\n", in, hid, num_blocks);

    cudaMemcpy(input_cuda, net->input_units, (in + 1) * sizeof(float),
               cudaMemcpyHostToDevice);
    cudaMemcpy(input_hidden_cuda, input_weights_one_dim,
               (in + 1) * (hid + 1) * sizeof(float), cudaMemcpyHostToDevice);

    PROFILE((
        bpnn_layerforward_CUDA<<<grid, threads>>>(input_cuda, output_hidden_cuda,
                                                  input_hidden_cuda,
                                                  hidden_partial_sum, in, hid)
    ));

    cudaThreadSynchronize();

    cudaError_t error = cudaGetLastError();
    if (error != cudaSuccess) {
        printf("bpnn kernel error: %s\n", cudaGetErrorString(error));
        exit(EXIT_FAILURE);
    }
	
	stopwatch_stop(&sw);
    	printf("GPU Computation executed in (ms): %lf\n", 1000 * get_interval_by_sec(&sw));

	stopwatch_start(&sw);
    cudaMemcpy(partial_sum, hidden_partial_sum,
               num_blocks * WIDTH * sizeof(float), cudaMemcpyDeviceToHost);

	stopwatch_stop(&sw);
    	printf("Partial sum copied in (ms): %lf\n", 1000 * get_interval_by_sec(&sw));

	stopwatch_start(&sw);
    for (int j = 1; j <= hid; j++) {
        sum = 0.0;
        for (int k = 0; k < num_blocks; k++) {
            sum += partial_sum[k * hid + j - 1];
        }
        sum += net->input_weights[0][j];
        net->hidden_units[j] = float(1.0 / (1.0 + exp(-sum)));
    }
    	
	stopwatch_stop(&sw);
    	printf("For loop after gpu computation executed in (ms): %lf\n", 1000 * get_interval_by_sec(&sw));
#endif


	stopwatch_start(&sw);
    bpnn_layerforward(net->hidden_units, net->output_units, net->hidden_weights,
                      hid, out);
	stopwatch_stop(&sw);
    	printf("BPNN Layerforward executed in (ms): %lf\n", 1000 * get_interval_by_sec(&sw));
	
	stopwatch_start(&sw);
    bpnn_output_error(net->output_delta, net->target, net->output_units, out,
                      &out_err);
	stopwatch_stop(&sw);
    	printf("BPNN Output error executed in (ms): %lf\n", 1000 * get_interval_by_sec(&sw));

	stopwatch_start(&sw);
    bpnn_hidden_error(net->hidden_delta, hid, net->output_delta, out,
                      net->hidden_weights, net->hidden_units, &hid_err);
	stopwatch_stop(&sw);
    	printf("BPNN Hidden error executed in (ms): %lf\n", 1000 * get_interval_by_sec(&sw));

	stopwatch_start(&sw);
    bpnn_adjust_weights(net->output_delta, out, net->hidden_units, hid,
                        net->hidden_weights, net->hidden_prev_weights);
	stopwatch_stop(&sw);
    	printf("BPNN Adjust weights executed in (ms): %lf\n", 1000 * get_interval_by_sec(&sw));

#ifdef CPU

	stopwatch_start(&sw);
    bpnn_adjust_weights(net->hidden_delta, hid, net->input_units, in,
                        net->input_weights, net->input_prev_weights);
	stopwatch_stop(&sw);
    	printf("CPU BPNN ADJUST WEIGHTS executed in (ms): %lf\n", 1000 * get_interval_by_sec(&sw));

#endif


#ifdef GPU

	stopwatch_start(&sw);
    cudaMalloc((void **)&hidden_delta_cuda, (hid + 1) * sizeof(float));
    cudaMalloc((void **)&input_prev_weights_cuda,
               (in + 1) * (hid + 1) * sizeof(float));

    cudaMemcpy(hidden_delta_cuda, net->hidden_delta, (hid + 1) * sizeof(float),
               cudaMemcpyHostToDevice);
    cudaMemcpy(input_prev_weights_cuda, input_weights_prev_one_dim,
               (in + 1) * (hid + 1) * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(input_hidden_cuda, input_weights_one_dim,
               (in + 1) * (hid + 1) * sizeof(float), cudaMemcpyHostToDevice);
	
	stopwatch_stop(&sw);
    	printf("Data copied from host to device executed in (ms): %lf\n", 1000 * get_interval_by_sec(&sw));
    
	stopwatch_start(&sw);	
    PROFILE((
        bpnn_adjust_weights_cuda<<<grid, threads>>>(
            hidden_delta_cuda, hid, input_cuda, in, input_hidden_cuda,
            input_prev_weights_cuda)
    ));
    	
	stopwatch_stop(&sw);
    	printf("BPNN ADJUST WEIGHTS CUDA executed in (ms): %lf\n", 1000 * get_interval_by_sec(&sw));

	stopwatch_start(&sw);
    cudaMemcpy(net->input_units, input_cuda, (in + 1) * sizeof(float),
               cudaMemcpyDeviceToHost);
    cudaMemcpy(input_weights_one_dim, input_hidden_cuda,
               (in + 1) * (hid + 1) * sizeof(float), cudaMemcpyDeviceToHost);

    	stopwatch_stop(&sw);
    	printf("Data copied from device to host executed in (ms): %lf\n", 1000 * get_interval_by_sec(&sw));

	stopwatch_start(&sw);
    cudaFree(input_cuda);
    cudaFree(output_hidden_cuda);
    cudaFree(input_hidden_cuda);
    cudaFree(hidden_partial_sum);
    cudaFree(input_prev_weights_cuda);
    cudaFree(hidden_delta_cuda);
    	
	stopwatch_stop(&sw);
    	printf("CUDA FREE executed in (ms): %lf\n", 1000 * get_interval_by_sec(&sw));

	stopwatch_start(&sw);
    free(partial_sum);
    free(input_weights_one_dim);
    free(input_weights_prev_one_dim);
	stopwatch_stop(&sw);
    	printf("FREE executed in (ms): %lf\n", 1000 * get_interval_by_sec(&sw));
    	

#endif
}
