#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "backprop.h"
#include <string.h>
#include <unistd.h>
#include "../common/common.h"

extern void bpnn_train_cuda(BPNN *net, float *eo, float *eh);
extern int load(BPNN *net);

int layer_size = 0;

void backprop_face() {
    BPNN *net;
    int i;
    float out_err, hid_err;

	stopwatch sw;
	stopwatch_start(&sw);
    net = bpnn_create(layer_size, 16, 1); // (16, 1 can not be changed)
	stopwatch_stop(&sw);
    	printf("BPNN Create executed in (ms): %lf\n", 1000 * get_interval_by_sec(&sw));
    printf("Input layer size : %d\n", layer_size);
    	stopwatch_start(&sw);
    load(net);
	stopwatch_stop(&sw);
    	printf("NET Load executed in (ms): %lf\n", 1000 * get_interval_by_sec(&sw));
    // entering the training kernel, only one iteration
    printf("Starting training kernel\n");
   	stopwatch_start(&sw);
    bpnn_train_cuda(net, &out_err, &hid_err);
	stopwatch_stop(&sw);
    	printf("BPNN Train cuda executed in (ms): %lf\n", 1000 * get_interval_by_sec(&sw));
    if (getenv("OUTPUT")) {
        bpnn_save(net, "outputShort.dat");
    } else if ( getenv("OUTPUT2")) {
	bpnn_save(net, "outputMedium.dat");
    } else if ( getenv("OUTPUT3")) {
	bpnn_save(net, "outputLong.dat");
    }
    stopwatch_start(&sw);
    bpnn_free(net);
	stopwatch_stop(&sw);
    	printf("BPNN FREE executed in (ms): %lf\n", 1000 * get_interval_by_sec(&sw));
    printf("Training done\n");
}

void run(int argc, char **argv) {
    if (argc != 2) {
        fprintf(stderr, "usage: backprop <num of input elements>\n");
        exit(1);
    }
	stopwatch sw;
	stopwatch_start(&sw);
    layer_size = atoi(argv[1]);
    	stopwatch_stop(&sw);
    	printf("Parse executed in (ms): %lf\n", 1000 * get_interval_by_sec(&sw));
    if (layer_size % 16 != 0) {
        fprintf(stderr, "The number of input points must be divided by 16\n");
        exit(1);
    }

    int seed = 7;
    stopwatch_start(&sw);
    bpnn_initialize(seed);
    stopwatch_stop(&sw);
    printf("BPNN Initialize executed in (ms): %lf\n", 1000 * get_interval_by_sec(&sw));

    stopwatch_start(&sw);
    backprop_face();
    stopwatch_stop(&sw);
    printf("BACKPROP_FACE is executed in (ms): %lf\n", 1000 * get_interval_by_sec(&sw));
}
