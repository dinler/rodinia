#!/usr/bin/env julia

using CUDA, NVTX
using Printf

include("../../common/julia/crand.jl")
const rng = LibcRNG()

include("backprop.jl")
include("backprop_cuda_kernel.jl")

const OUTPUT = haskey(ENV, "OUTPUT")
const OUTPUT2 = haskey(ENV, "OUTPUT2")
const OUTPUT3 = haskey(ENV, "OUTPUT3")


function backprop_face(layer_size)
    @time net = bpnn_create(layer_size, 16, 1) # (16, 1 cannot be changed)
	println("BPNN Create executed")
    println("Input layer size : ", layer_size)
	@time begin
    units = net.input_units
    for i = 2:layer_size+1
        units[i] = float(rand(rng)) / RAND_MAX
    end
	end
	println("NET LOAD executed")
    # Entering the training kernel, only one iteration.
    println("Starting training kernel")
    @time bpnn_train_cuda(net)
	println("BPNN Train cuda executed")

    if OUTPUT
        bpnn_save(net, "outputShort.dat")
    elseif OUTPUT2
	bpnn_save(net, "outputMedium.dat")
    elseif OUTPUT3
	bpnn_save(net, "outputLong.dat")
    end

    println("Training done")
end

function main(args)
    if length(args) != 1
        println(stderr, "usage: backprop <num of input elements>");
       # exit(1)
    end
	if length(args) == 0
		layer_size = 65536
	else
    		@time layer_size = parse(Int, args[1])
		println("Parse executed")
	end

    if layer_size % 16 != 0
        @printf(stderr, "The number of input points must be divisible by 16\n")
        #exit(1)
    end
    @time bpnn_initialize(7)
	println("BPNN Initialize executed")
    @time backprop_face(layer_size)
    	println("BACKPROP_FACE is executed")
end


if abspath(PROGRAM_FILE) == @__FILE__
    NVTX.stop()
    @time main(ARGS)
	println("First main executed")

	if haskey(ENV, "TWICE")
    		@time main(ARGS)
    		println("Second main executed")
        end

    if haskey(ENV, "PROFILE")
        # warm up
        for i in 1:5
            main(ARGS)
            GC.gc()
        end

       # empty!(CUDAnative.compilecache)

        NVTX.@activate begin
            for i in 1:5
                GC.gc(true)
            end
            main(ARGS)                                       # measure compile time
            for i in 1:5
                GC.gc(true)
            end
 	
            for i in 1:5
            	CUDA.@profile NVTX.@range "host" main(ARGS)   # measure execution time
       	    end
	end
    end
end
