#!/usr/bin/env julia

#Base.reinit_stdio()
#Base.init_depot_path()
#Base.init_load_path()


using CUDA, NVTX
using LLVM


#empty!(LOAD_PATH)
#empty!(DEPOT_PATH)


# AHMET
#using BenchmarkTools

include("common.jl")
include("lud_kernel.jl")

function main(args)
	
    verify = haskey(ENV, "OUTPUT")
    if length(args) == 1
        try
		println("PARSE EXECUTED \n")
            @time matrix_dim, input_file = parse(Int, args[1]), nothing
        catch
		println("NO PARSE EXUCUTED \n")
            @time matrix_dim, input_file = nothing, args[1]
        end
    else
	println("DEFAULT INPUT EXECUTED\n")
        @time matrix_dim, input_file = 32, nothing
    end
	
    if input_file != nothing
        println("Reading matrix from file $input_file")
        @time matrix, matrix_dim = create_matrix_from_file(input_file)
    elseif matrix_dim > 0
        println("Creating matrix internally size=$(matrix_dim)")
        @time matrix = create_matrix(matrix_dim)
    else
        error("No input file specified!")
    end

    if verify
        println("Before LUD")
        @time matrix_copy = copy(matrix)
    end

	println("d_matrix has been created\n")
        @time d_matrix = CuArray(matrix)
        println("lud_cuda has been executed\n")
	@time lud_cuda(d_matrix, matrix_dim)
        println("matrix has been created\n")
	@time matrix = Array(d_matrix)

    if verify
        println("After LUD")
        println(">>>Verify<<<<")
        @time lud_verify(matrix_copy, matrix, matrix_dim)
    end

end

# FIXME: for now we increase the unroll threshold to ensure that the nested loops in the
# kernels are unrolled as is the case for the CUDA benchmark. Ideally, we should annotate
# the loops or the kernel(s) with the @unroll macro once it is available.
LLVM.clopts("--unroll-threshold=1200")

if abspath(PROGRAM_FILE) == @__FILE__
	@time NVTX.stop()
	@time main(ARGS)
    println("FIRST MAIN EXECUTED\n")
	if haskey(ENV, "TWICE")
		@time main(ARGS)
		println("SECOND MAIN EXECUTED\n")
	end
	#mt2 = @elapsed begin
    #main(ARGS)
#	end
#	println("Time mt2: $(mt2)")
    if haskey(ENV, "PROFILE")
        # warm up
        for i in 1:5
            main(ARGS)
            GC.gc()
        end

        #empty!(CUDAnative.compilecache)

        NVTX.@activate begin
            for i in 1:5
                GC.gc(true)
            end
            main(ARGS)                                       # measure compile time
            for i in 1:5
                GC.gc(true)
            end
         
            for i in 1:5
	   	CUDA.@profile NVTX.@range "host" main(ARGS)   # measure execution time
            end
	end
    end
end

#empty!(LOAD_PATH)
#empty!(DEPOT_PATH)
