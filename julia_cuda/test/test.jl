#!/usr/bin/env julia

using CUDA, NVTX

using Printf

function kernel(x, n)
	tid = threadIdx().x + (blockIdx().x-1) * blockDim().x
	for i = tid:blockDim().x*gridDim().x:n
		x[i] = 5
		@sprintf("d\n", x[i])
	end
	return
end

function  main(args) 
 
#	@printf("hello")
	work_mem_d = CuArray{Float32}(undef, 7);
	@cuda blocks=1 threads=64 kernel(work_mem_d, 7)
	
end



